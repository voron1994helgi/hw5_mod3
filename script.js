// *Task1
function numOfDivision (a, b){
    console.log(a / b);
}
numOfDivision(10, 2);

// *Task2

let firstNum = prompt("Enter first number, please.");
let secondNum = prompt("Enter second number, please.");


while (isNaN(firstNum) || isNaN(secondNum)){
  alert ("Not a number. Try again");
  firstNum = prompt("Enter first number, please.");
  secondNum = prompt("Enter second number, please.");
}
    
let operation = prompt("Choose operation: +, -, *, /: ");

while (operation !== "+" && operation !== "-" && operation !== "*" && operation !== "/") {
    alert("This operation doesn't exist. Try again");
    operation = prompt("Enter one of the operations: +, -, *, /: ");
}

function mathOperations (firstNum, secondNum){
 let result;
 switch(operation){
    case "+":
        result = firstNum + secondNum;
        break;
    case "-":
        result = firstNum - secondNum;
        break;
    case "*":
        result = firstNum * secondNum;
        break;
    case "/":
       result = firstNum / secondNum;
       break;
 }
console.log(result);
}

mathOperations(parseFloat(firstNum), parseFloat(secondNum));


// *Task3

function factorial(x) {
    if (x < 0) return ;
    if (x === 0) return 1;
    return x * factorial(x - 1);
}

console.log(factorial(3));
console.log(factorial(4));
